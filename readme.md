# Heartbeatz Framework

### Overview
The following xcFramework and demo application showcases the features and data endpoints available for the Heartbeatz interface with the use of Heartbeatz Android Library.

This code collection enables the developer to connect to Apple Watch via the Heartbeatz interfaces delivered by North Pole Engineering.

Requirements:
- iOS 13+ / watchOS 6+
- Xcode 12+
- Swift 5+
- Physical Apple Watch and iPhone for testing

Control Methods:
- SwiftUI @published objects
- Delegate Methods
- Notification Methods

Note: its best to pick one methodology. Mixing and matching delegates and notifications can lead to odd behavior.

## Demo App (Delegate Method)
Included in the package is the simple swift delegate method project. This project has 2 distinct sections worth discussing. The first is permissions. The framework is built to use permissions objects to set and query HealthKit and BLE permissions, before enableing the main connection and workout section. It is best to ask for permissions before using healthKit and Bluetooth capabilities. 

The second section is where most of the framework resides. The ConnectionModel once initiated, will scan for compatable devices, create a connection, and then control data collection. User control of workouts can be made via Apple Watch or through the Heartbeatz Android Library.

Inside the demo app HeartbeatzDemo, there are 3 viewControllers of note in the Watch Extension folder. 
- HKController
- BLEController
- ScannerController
- HeartbeatsDataView (plus sub views).

### HKController
The first task is to create a shared instance of the HBTZPermissions class. This will help set the permissions code to handle asyc callbacks and/or seperate permissions steps based on UI neads. 

`let npePermissions = HBTZPermissions.shared `

The 3 main functions of the HBTZPermissions class are to create a stored object for displaying the HealthKit permissons popup. Then requesting authorization of needed permission for the ConnectionModel to function. After a request for Authorization. The state of indiviual write permissions can be queried. Note HealthKit has no method to query individual Read permissions. 

**Setting Read and Write Permissions**

```
    npePermissions.HKPremissionsWrite = [.workoutInfo, .distanceCycle, .distanceTreadmill]

    npePermissions.HKPremissionsRead = [.workoutInfo, .heartrate, .activeEnergy, .basilEnergy, .distanceCycle, .distanceTreadmill]
```

**Permission Callbacks**
```
    setupHealthKit() { success in }
    requestPermissions() { success, error in }
    queryPermissionStatus() { success, errors in }
```

### BLEController

The BLEController is very similar to the HKController but much simpliler, as it only has one callback 

**Permission Callbacks**
```
    checkBLEPermissions { success in }
```
### ScannerController
This allows the framework to scan for available Heartbeatz Devices/Library Instances. The sample demo app has a swift file 'ScannerController.swift' that shows how to display the device collected via the frameworks. The data shows stored (ie, connected to previously) first. Then shows discovered (ie, new devices) next. And finally stored, but not available. The later is grayed out.

`let multiDevice = MultiDevice.shared `

Selecting an available device will internally store the device until ConnectionModel is called and the start connection function is called 'discoverOrConnnectHeartbeatz(useSelectedUserDevice: true)'. This allows the UI to be broken up into seperat screens and the developer to choose how and when to start the connection. (see override func awake in MainDisplayController for new MultiDevice connection funcion). NOTE: starting scanning is best called from willActivate() func. This lets the call happen when the screen is returned to via navigation.

**Starting Scanning**

```
multiDevice.scanForDeviceTypes = [.heartbeatz, .heartbeatzLib, .heartbeatzCable]

override func willActivate() {
    // starts scanning for heartbeatz devices/library
    multiDevice.startListening()
}
```
**Selecting Device**

```
self.multiDevice.saveSelectedDeviceAndStopListening(selected: self.displayStack[rowIndex].device)
```

**Scanner Delegate**
```
didDiscoverDevices(availableList: [UserStoredDevices], savedList: [UserStoredDevices], discoveredList: [UserStoredDevices], current: UserStoredDevices?) { .... }
```

### HeartbeatsDataView
The HeartbeatsDataView is an inheritable view that complies with the ConnectionModelDelegate method. The framework handles scanning, connection, workouts, saving data all via a few delegate methods. This WKInterfaceController extension make reuse in other pages simple. In this demo example, the HeartbeatsDataView is used in 4 of the display page samples. MenuController, MainDisplayController, ExtraDisplayController, and finally SummaryController. These are sample pages, but have been built to show capabilities of the framework.

    
Set Class Delegate on Object level `ConnectionModelDelegate`

Set shared instance at class level `let connectionModel = ConnectionModel.sharedConnectionModel`
  
Set the delegate at `awake(withContext context: Any?)` or if using Many screens like the example, it best to set delegate on `willActivate()`as the screens change, the delegate moves to the newest, and the override functions can be used for specific page needs.


NEW: With addition of MultiDevice, actual device connection calls were taken out of frameworks init function and added to its own function for flexability and ability to pass parameters after init. This will need to be added to current code using 1.9 + of the frameworks
```
DispatchQueue.main.async {
    //NOTE: if using the multiDevice handler set true, else false to use autoconnect to any Heartbeatz device
    //If MultiDevice class is used to set the device to connect to, this function will initate the connection.
    self.connectionModel.discoverOrConnnectHeartbeatz(useSelectedUserDevice: true)
}
```

**Delegate Methods**
```
    func didUpdateMetrics()
    func didUpdateSessionState(new: SessionState, prev: SessionState)
    func didUpdateWorkoutState(new: WorkoutSessionState, prev: WorkoutSessionState)
    func didCreateSummary(summary: SummaryObject)
    
    func didReceiveMessage(message: String, key: Int, state: LongMessageState, error: LongMessageStateError?)
    func didSendMessage(state: LongMessageState, error: LongMessageStateError?)
    
    func didUpdateHeartbeatzBLEState(state: HeartbeatzBLEState)


```

**didUpdateMetrics**
This delegate updates during an active workout when HealthKit parses data from the watch and from Android Library. Any workout metric variable listed in the ConnectionModel Reference can be evaled and or displayed. There are many ways to access data inside ConnectionModel. The easiest is to query the simple Raw variable for a metric. This variable is a native Double. The framework provides a formatting structure to take the raw doubles and fine tune the number to display string. More about the String Formatters in ConnectionModel Format Structures.

Note: This framework is built for SwiftUI and Regular Swift. This causes some issues with updating the UI while the watch screen is inactive. If the UI is not using SwiftUI, then seperate simple vars are more effiecent to display and be used in the UI. 

**Example:**
```
    func didUpdateMetrics(){
        let workoutState = self.connectionModel.sessionState

        let bpm = ConnectionModel.formatIntValue(source: self.connectionModel.heartRateRaw)
        let tCal = ConnectionModel.formatDoubleValue(source: self.connectionModel.totalCaloriesRaw, places: 2)
        let aCal = ConnectionModel.formatDoubleValue(source: self.connectionModel.activeCaloriesRaw, places: 2)

        print("BPM: \(bpm) State: \(workoutState) Cal: \(aCal) tCal: \(tCal)")
```

**didUpdateSessionState**

The sessionState lets the UI know what state the framework is at anytime. The framework sets the sessionState depending on device connection state, as well as what part of a workout the user is in. The function informs the UI what is the current state as well as the previous state to custom build the needed UI screens around. 

The delegate can use a simple or extended internal logic. In most UI implementations, while the user is in a workout, pausing and resuming do not need to change the screen. The default state for SessionState is to use simple format. To extend SessionState to include pause, resume, starting, and ending states. Use `useExpandedSessionState = true`

**Example:**

```
    func didUpdateSessionState(new: SessionState, prev: SessionState) {
        switch new {
        case .scanning:
            print("UI Session: Scanning")
        case .initializing:
            print("UI Session: Initalizing")
        case .initialized:
            print("UI Session: Initalized")
        case .idle:
            print("UI Session: Idle")
        case .active:
            print("UI Session: Active Workout")
        case .paused:  // Extended Only
            print("UI Session: Paused Workout")
        case .complete:
            print("UI Session: Completed Workout")
         case .ending:  // Extended Only
            print("UI Session: Ending Workout Session")
        case .starting: // Extended Only
            print("UI Session: Starting Workout Session")
        default:
            print("UI Session: Unknown")
        }
    }

```

**didUpdateWorkoutState**
Similar to sessionState, while the workout is active, there are a number of states that can be observed. The WorkoutSessionState is used to infor the UI of changes that can come from many sources. The foremost, control from the Android Library. Healthkit Sessions can also be changed via other 3rd party workout apps. 

**Example:**

```
    func didUpdateWorkoutState(new: WorkoutSessionState, prev: WorkoutSessionState) {
        switch new {
        case .running:
            print("UI Workout: Running")
        case .paused:
            print("UI Workout: Paused")
        case .idle:
            print("UI Workout: Idle")
        case .stopped:
            print("UI Workout: Stopped")
        case .none:
            print("UI Workout: None")
        default:
            print("UI Workout: Unknown")
        }
    }

```

**didCreateSummary**
At the end of a workout, the framework captures a list of averages and totals into an swift object. 
**Example:**

```
    func didCreateSummary(summary: SummaryObject) {
       *see summaryObject under ConnectionModel
    }

```
**didReceiveMessage**
The Heartbeatz Android Library has the ability to send & receive custom messages back and forth between the watch and android library. This feature is only availible with use of Heartbeatz Android Library.
**Example:**

```
    func didReceiveMessage(message: String, key: Int, state: LongMessageState, error: LongMessageStateError?) {
        DispatchQueue.main.async {
            if state == .received {
                print("UI Long Message: \(message) Key: \(key)")
            } else {
                if error == nil {
                    print("UI Long Receive State: \(state)")
                } else {
                    print("UI Long Receive State: \(state) Error: \(String(describing: error))")
                }
            }
        }
    }

```

**didSendMessage**
The Heartbeatz Android Library has the ability to send & receive custom messages back and forth between the watch and android library. This feature is only availible with use of Heartbeatz Android Library. 
**Example:**

```
    func didReceiveMessage(message: String, key: Int, state: LongMessageState, error: LongMessageStateError?) {
        DispatchQueue.main.async {
            if state == .received {
                print("UI Long Message: \(message) Key: \(key)")
            } else {
                if error == nil {
                    print("UI Long Receive State: \(state)")
                } else {
                    print("UI Long Receive State: \(state) Error: \(String(describing: error))")
                }
            }
        }
    }

```


# ConnectionModel Vars & Functions

**INFORMATIONAL**
```
    var currentFrameworksVersion: String
    var currentFirmwareVersion: String
    var uniqueDeviceId: [UInt8]?
    var currentVersion: String
    var uniqueSessionId: UInt16
```


**DELEGATE AND INSTANCE**
```
    var delegate: ConnectionModelDelegate?
    static let sharedConnectionModel = ConnectionModel() // Legacy
```

**HEARTBEATZ DEVICE**
```
    var heartbeatzBLEState: HeartbeatzBLEState = .unknown

    enum HeartbeatzBLEState {
        case disconnected
        case connecting
        case connected
        case unknown
    }
```


**CONTROL FLAGS**
```
    var autoConnectModel = true
    var useHaptic: HapticControl = .all
    var disableScanning = false
    var keepConnectionInIdle = true
    var showCaloriesDuringIdle = true
    var showCommsPrint = false
    var showDebugPrint = false
    var shouldUseWatchConnectionComms = false
    var watchFaceDimmedWorkoutActive = false
    var pauseWorkoutOnDisconnect = false
    var useMultiDevice: Bool = false
    var useDelegateMethod = false
```

**HEALTHKIT**
```
    let healthStore = HKHealthStore()
    let configuration = HKWorkoutConfiguration()
    var session: HKWorkoutSession!
    var builder: HKLiveWorkoutBuilder!
    @Published var lastSession: HKWorkoutSession!
```


**PRODUCT**
```
    var lockerInst: LockerDevice
    var waspInst: WaspBroadcastDevice
    @Published var deviceName: String?
```

**SESSION STATES**
```
    @Published var sessionState: SessionState = .none
    @Published var workoutSessionState: WorkoutSessionState = .none
```



**CONFIGURE**
```
    var minRssi: Int8 = -80
    var minProxyRssi: Int8 = -40
    var androidLibrary: AndroidTXShunt = .none
    var scanForDeviceTypes: [AweProductType]
    
```


**WORKOUT RELATED**
```
    @Published var workoutElements: WorkoutElements
    @Published var activitySummary: HKActivitySummary?
    @Published var equipmentTime: TimeInterval?

    @Published var workoutTypes: [WorkoutType]
    @Published var workoutType: WorkoutType?
    
    var phoneCurrHr: Measurement<UnitCadence>?
    var activeCalories: Measurement<UnitEnergy>?
    var totalCalories: Measurement<UnitEnergy>?
    var deviceTotalCalories: Measurement<UnitEnergy>?
    var totalDistance: Measurement<UnitLength>?
    var cadence: Measurement<UnitCadence>?
    var currAvgHr: Measurement<UnitCadence>?
    var avgCadence: Measurement<UnitCadence>?
    var avgBikeCadence: Measurement<UnitCadence>?
    var avgPower: Measurement<UnitPower>?
    var avgSpeed: Measurement<UnitSpeed>?
    var avgPace: Measurement<UnitDuration>?
    var avgBikeSpeed: Measurement<UnitSpeed>?
    var instantPower: Measurement<UnitPower>?
    var instantSpeed: Measurement<UnitSpeed>?
    var instantPace: Measurement<UnitDuration>?
    var strokeCount: Measurement<UnitCount>?
    var sessionTimeSwift: TimeInterval?

    var pauseInterval:TimeInterval = 0.0

    var currWorkoutStart: Date?
    var prevHrValue: Double = 0

    let heartRateUnit = HKUnit.count().unitDivided(by: HKUnit.minute())

 
```

   
**WORKOUT SUMMARY**
```
    var currentSummary: SummaryObject?
    var sessionSummaries: [SummaryObject] = []
    var onSaveMetaData:[String:Any] = [:]
    var sumTitle: String = ""
    
    struct SummaryObject: Any {
        public var activity: Int = 73
        public var title: String = ""
        public var unitMetric: Bool = true
        public var time: TimeInterval = 0.0
        public var avgHeartRate: Measurement<UnitCadence> = Measurement(value: 0.0, unit: UnitCadence.beatsPerMinute)
        public var activeCalories: Measurement<UnitEnergy> = Measurement(value: 0.0, unit: UnitEnergy.calories)
        public var totalCalories: Measurement<UnitEnergy> = Measurement(value: 0.0, unit: UnitEnergy.calories)
        public var deviceTotalCalories: Measurement<UnitEnergy> = Measurement(value: 0.0, unit: UnitEnergy.calories)
        public var totalDistance: Measurement<UnitLength> = Measurement(value: 0.0, unit: UnitLength.meters)
        public var avgSpeed: Measurement<UnitSpeed> = Measurement(value: 0.0, unit: UnitSpeed.metersPerSecond)
        public var avgPower: Measurement<UnitPower> = Measurement(value: 0.0, unit: UnitPower.watts)
        public var avgBikeCadence: Measurement<UnitCadence> = Measurement(value: 0.0, unit: UnitCadence.revolutionsPerMinute)
        public var avgBikeSpeed: Measurement<UnitSpeed> = Measurement(value: 0.0, unit: UnitSpeed.metersPerSecond)
        public var avgCadence: Measurement<UnitCadence> = Measurement(value: 0.0, unit: UnitCadence.revolutionsPerMinute)
        public var avgPace: Measurement<UnitDuration> = Measurement(value: 0.0, unit: UnitDuration.seconds)
        public var strokeCount: Measurement<UnitCount> = Measurement(value: 0.0, unit: UnitCount.counts)
        public var workoutStart: Date?
        public var workoutEnd: Date?
    }

```

**FUNCTIONS**
``` 
    // Workout Controls
    func configureWorkout(auto: Bool, currentWorkoutType: WorkoutType) -> Bool
    func resumeWorkout() 
    func pauseWorkout()
    func stopWorkout(saveWorkout: Bool, disconnectAfterSave: Bool, metaData: [String:Any]) {
    func cancelWorkout()
    func recoverSession() 

    //For use with Extension Delegate
    func enterActive() {
    func enterInactive()
    func enterBackground()

    // Disconnect from Android Library
    func disconnectHeartbeatz(stopScanning: Bool = true, delay: Double = 15.0) {
    func resetDisconnectHeartbeatz()
    
    //Helpers for Display of Metrics
    func getTimeValue(source: TimeInterval?) -> String
    func getPedalBalanceValue(source: Measurement<UnitPercent>?) -> String
    func getInclineValue(source: Measurement<UnitPercent>?) -> String
    func getCalorieValue(source: Measurement<UnitEnergy>?) -> String
    func getHrValue(source: Measurement<UnitCadence>?) -> String
    func getAscDscValue(source: Measurement<UnitLength>?) -> String
    func getDistanceValue(source: Measurement<UnitLength>?) -> String
    func getPowerValue(source: Measurement<UnitPower>?) -> String
    func getCountValue(source: Measurement<UnitCount>?) -> String
    func getCadenceValue(source: Measurement<UnitCadence>?) -> String
    func getSpeedValue(source: Measurement<UnitSpeed>?) -> String
    func getPaceUnits(source: Measurement<UnitDuration>?) -> String
    func getPaceValue(source: Measurement<UnitDuration>?) -> String

```

**PROTOCOLS**
```
    protocol ConnectionModelDelegate: AnyObject {
        func didUpdateMetrics()
        func didUpdateSessionState(new: SessionState, prev: SessionState)
        func didUpdateWorkoutState(new: WorkoutSessionState, prev: WorkoutSessionState)

        func didCreateSummary(summary: SummaryObject)
    
        func didReceiveMessage(message: String, key: Int, state: LongMessageState, error: LongMessageStateError?)
        func didSendMessage(state: LongMessageState, error: LongMessageStateError?)

        func didUpdateHeartbeatzBLEState(state: HeartbeatzBLEState)

    }
```

**ENUMS**
```
    enum ElementType: UInt8 {
        case none
        case time
        case activeCalories
        case totalCalories
        case power
        case speed
        case pace
        case distance
        case heartrate
        case cadence
        case avgPower
        case avgSpeed
        case avgPace
        case avgHeartrate
        case avgCadence
        case avgBikeCadence
        case avgBikeSpeed
        case ascent
        case descent
        case incline
        case steps
        case blank
    }

    enum SessionState {
        case none
        case initializing
        case initialized
        case idle
        case scanning
        case configure
        case creating
        case starting
        case restoring
        case active
        case paused
        case ending
        case complete
        case discarded
    }
    enum WorkoutSessionState {
        case none
        case idle
        case running
        case paused
        case ended
        case stopped
        case unknown
    }
    enum HapticControl {
        case none
        case warning
        case all
    }
    enum HeartbeatzBLEState {
        case disconnected
        case connecting
        case connected
        case unknown
    }

```

**NOTIFICATIONS**
Note: SessionState and WorkoutSessionState are covered in delegate method. But the whole framework can be set to use Notification observers. 
```
    static let sessionStateEvent = Notification.Name("sessionStateEvent")
    static let longMessageSendEvent = Notification.Name("longMessageSendEvent")
    static let longMessageReceiveEvent = Notification.Name("longMessageReceiveEvent")
    static let communicationsEvent = Notification.Name("communicationsEvent")
    static let workoutSaveEvent = Notification.Name("workoutSaveEvent")
    static let workoutDiscardEvent = Notification.Name("workoutDiscardEvent")
    static let workoutEndEvent = Notification.Name("workoutEndEvent")
    static let workoutStartEvent = Notification.Name("workoutStartEvent")
    static let workoutPauseEvent = Notification.Name("workoutPauseEvent")
    static let workoutResumeEvent = Notification.Name("workoutResumeEvent")
    static let workoutSessionState = Notification.Name("workoutSessionState")
    static let libraryRequestedDisconnect = Notification.Name("libraryRequestedDisconnect")
    static let warnAboutUnknownDisconnect = Notification.Name("warnAboutUnknownDisconnect")
    static let heartbeatzReconnectedSuccess = Notification.Name("heartbeatzReconnectedSuccess")
    static let hbConnectedEvent = Notification.Name("hbConnected")

```


# MultiDevice

**Configure**
```
    var selectedUserStoredDevice: selectedUserStoredDevice?
    var delegate: MultiDeviceDelegate?
    var minRssi: Int8 = -120
    var scanForDeviceTypes: [AweProductType] = [.heartbeatz, .heartbeatzClub, .heartbeatzSeries2, .heartbeatzBcast, .heartbeatzHubb, .heartbeatzRunn, .heartbeatzCable, .heartbeatzLib, .heartbeatzGem]
```

**Functions**
```
    func startListening()
    func saveSelectedDeviceAndStopListening(selected: UserStoredDevices )
    func listUserStoredDevices(scan: Bool)
    func editUserStoredDevices(device: UserStoredDevices?)
    func forgetUserStoredDevices(device: UserStoredDevices?)
    func discconnectUserStoredDevices(device: UserStoredDevices?, forget: Bool)

```

# HBTZPermissions

**Configure**
```
    static let shared = HBTZPermissions()
    var HKPremissionsWrite: [HealthKitPermissions] = [.workoutInfo, .distanceCycle, .distanceTreadmill]
    var HKPremissionsRead: [HealthKitPermissions] = [.workoutInfo, .heartrate, .activeEnergy, .basilEnergy, .distanceCycle, .distanceTreadmill]
    var shownHKPermissions: Bool
    var shownBLEPermissions: Bool
    var secondsToCheckBLEPermissions: Int = 12
```

**Functions**
```
    func setupHealthKit(completion: @escaping (Bool)->())
    func requestPermissions(completion: @escaping (Bool, Error?)->())
    func queryPermissionStatus(completion: @escaping (Bool,[String])->())
    func checkBLEPermissions(completion: @escaping (Bool, CBManagerAuthorization)->()) 

```
