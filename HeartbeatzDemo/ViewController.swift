//
//  ViewController.swift
//  HeartbeatzDemo
//
//  Created by Steffin Fox Griswold on 8/2/21.
//

import UIKit
import HeartbeatzFW

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        ConnectionModel.sharedConnectionModel.autoConnectModel = true
        ConnectionModel.sharedConnectionModel.minRssi = -80
        
        NotificationCenter.default.addObserver(self, selector: #selector(checkSessionState), name: Notification.Name("sessionStateEvent"), object: nil)
    }
    
    @objc func checkSessionState() {
        switch ConnectionModel.sharedConnectionModel.sessionState {
        case .scanning:
            print("Scanning")
        case .initializing:
            print("Initalizing")
        case .initialized:
            print("Initalized")
        case .idle:
            print("Idle")
        case .active:
            print("Active Workout")
        case .paused:
            print("Paused Workout")
        case .complete:
            print("Completed Workout")
        case .ending:
            print("External App ending Session")
        default:
            print("Unknown")
        }
    }
}

