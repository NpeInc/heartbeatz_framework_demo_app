//
//  ScannerRow.swift
//  HeartbeatzWatchDemo Extension
//
//  Created by Steffin Fox Griswold on 9/30/21.
//

import Foundation
import WatchKit
import HeartbeatzFW


class MenuRow: NSObject {
    @IBOutlet var icon: WKInterfaceImage!
    @IBOutlet var label: WKInterfaceLabel!

    func populate(){
        //print("populate: \(device.name)")
    }
    
}
