//
//  ScannerRow.swift
//  HeartbeatzWatchDemo Extension
//
//  Created by Steffin Fox Griswold on 9/30/21.
//

import Foundation
import WatchKit
import HeartbeatzFW


class ScannerRow: NSObject {
    @IBOutlet var deviceLabel: WKInterfaceLabel!
    @IBOutlet var statusLabel: WKInterfaceLabel!
    @IBOutlet var useLabel: WKInterfaceLabel!
    
    let dateFormatter = DateFormatter()
    
    var state: DeviceAvailabiltyState = .unknown
    var device: UserStoredDevices!

    func populate(){
        //print("populate: \(device.name)")
        if device.userName != "" {
            deviceLabel.setText(device.userName)
        } else {
            deviceLabel.setText(device.name)
        }
        switch state {
        case .available:
            statusLabel.setText("Available")
            statusLabel.setTextColor(.green)
        case .discovered:
            statusLabel.setText("Discovered")
            statusLabel.setTextColor(.green)
        default:
            statusLabel.setText("Not Found")
            statusLabel.setTextColor(.gray)
        }
        var timeString = "---"
        if device.lastUsed != 0 {
            let date: Date = Date(timeIntervalSince1970: device.lastUsed)
            dateFormatter.timeStyle = .short
            dateFormatter.dateStyle = .short
            timeString = dateFormatter.string(from: date)
        }
        useLabel.setText("Last Used: \(timeString)")
    }
    
}
