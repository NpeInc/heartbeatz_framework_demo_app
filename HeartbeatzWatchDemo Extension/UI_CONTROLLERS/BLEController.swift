//
//  BLEController.swift
//  HeartbeatzWatchDemo Extension
//
//  Created by Steffin Fox Griswold on 9/7/21.
//

import Foundation
import HeartbeatzFW
import WatchKit

class BLEController: WKInterfaceController {
    @IBOutlet var info: WKInterfaceLabel!
    @IBOutlet var action: WKInterfaceButton!
    
    let npePermissions = HBTZPermissions.shared
    
    override func awake(withContext context: Any?) {
        //The check below should timeout in x seconds to return false from OS.
        npePermissions.secondsToCheckBLEPermissions = 15
        
        if !npePermissions.shownBLEPermissions {
            npePermissions.checkBLEPermissions { success, state in
                print("Query BLE Permissions: \(success) state: \(state.rawValue)")
                self.info.setText("BLE Accepted: \(success) state: \(state.rawValue)")
                switch state {
                case .allowedAlways:
                    //yes
                    break
                case .denied:
                    // boo
                    break
                default:
                    //ask again?
                    break
                }
            }
        } else {
            info.setText("BLE Accepted: \(npePermissions.shownBLEPermissions)")
        }
        
    }
    override func willActivate() {
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }
}

