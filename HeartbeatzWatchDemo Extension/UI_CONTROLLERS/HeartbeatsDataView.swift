//
//  HeartbeatsDataView.swift
//  HeartbeatzWatchDemo Extension
//
//  Created by Steffin Fox Griswold on 11/16/21.
//

import WatchKit
import Foundation
import HeartbeatzFW

class HeartbeatzDataView: WKInterfaceController, ConnectionModelDelegate {
    
    var actionState: Bool = false
    let connectionModel = ConnectionModel.sharedConnectionModel
    var workoutTime = "00:00:00"
    var bpm = "0"
    var tCal = "0"
    var aCal = "0"
    var dvCal = "0"
    var tDist = "0"
    
    // not enough room to show on screen in simple format
    var avgHR = "0"
    var instSpeed = "0"
    var instPace = "0"
    var avgBikeSpeed = "0"
    var avgBikeCadence = "0"
    var cadence = "0"
    var avgCadence = "0"
    var power = "0"
    var avgPower = "0"
    var avgPace = "0"
    var strokeCount = "0"
    var count: Int = 0
    var crownValue = 1
    
    var workoutState: WorkoutSessionState = .unknown
    
    var mainScreenDisplay: String = ""
    var extraScreenDisplay: String = ""
    var summaryScreenDisplay: String = ""
    var displayScreen: Int = 0
    
    var connectionMessage: String = "Disconnect"
    
    override func awake(withContext context: Any?) {
        print("HeartbeatzDataView awake: isRunning: \(connectionModel.isRunning)")
        if !connectionModel.isRunning {
            connectionModel.minRssi = -120
            connectionModel.minProxyRssi = -80
            connectionModel.showCommsPrint = true
            connectionModel.showDebugPrint = false
            
            // Set new Delegate
            connectionModel.useDelegateMethod = true
            connectionModel.delegate = self
        }
        
        let sessionState = self.connectionModel.sessionState
        mainScreenDisplay = "Session State \(sessionState)"
        extraScreenDisplay = "No information"
    }
    
    func displayMetrics(){
        // used to populate local labels
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }
    
    
    //MARK: - DELEGATE CONNECTIONMODEL
    func didUpdateMetrics(){
        DispatchQueue.main.async { [self] in
            workoutTime = ConnectionModel.getTimeValue(source: connectionModel.sessionTimeSwift)
            
            workoutState = self.connectionModel.workoutSessionState
            bpm = ConnectionModel.getHrValue(source: connectionModel.phoneCurrHr)
            
            tCal = ConnectionModel.getCalorieValue(source: connectionModel.totalCalories)
            aCal = ConnectionModel.getCalorieValue(source: connectionModel.activeCalories)
            dvCal = ConnectionModel.getCalorieValue(source: connectionModel.deviceTotalCalories)
            tDist = ConnectionModel.getDistanceValue(source: connectionModel.totalDistance)
            
            // not enough room to show on screen in simple format
            avgHR = ConnectionModel.getHrValue(source: connectionModel.currAvgHr)
            instSpeed = ConnectionModel.getSpeedValue(source: connectionModel.instantSpeed)
            avgBikeSpeed = ConnectionModel.getSpeedValue(source: connectionModel.avgBikeSpeed)
            avgBikeCadence = ConnectionModel.getCadenceValue(source: connectionModel.avgBikeCadence)
            cadence = ConnectionModel.getCadenceValue(source: connectionModel.cadence)
            power = ConnectionModel.getPowerValue(source: connectionModel.instantPower)
            avgPower = ConnectionModel.getPowerValue(source: connectionModel.avgPower)
            
            //rower
            strokeCount = ConnectionModel.getCountValue(source: connectionModel.strokeCount)
            instPace = ConnectionModel.getPaceValue(source: connectionModel.instantPace)
            avgPace = ConnectionModel.getPaceValue(source: connectionModel.avgPace)
            avgCadence = ConnectionModel.getCadenceValue(source: connectionModel.avgCadence)
                
            displayMetrics()
        }
        
    }
    func didCreateSummary(summary: SummaryObject) {
        DispatchQueue.main.async {
            print("didCreateSummary")
            // remove current data display screens an jump to summary
            WKInterfaceController.reloadRootPageControllers(withNames: ["MenuController","SummaryController"], contexts: nil, orientation: .horizontal, pageIndex: 1)
        }
    }
    
    func didUpdateSessionState(new: SessionState, prev: SessionState) {
        DispatchQueue.main.async {
            switch new {
            case .scanning:
                print("UI Session: Scanning")
                // jump to scanner screen.
                WKInterfaceController.reloadRootPageControllers(withNames: ["Scanner"], contexts: nil, orientation: .horizontal, pageIndex: 0)
            case .initializing:
                print("UI Session: Initalizing")
            case .initialized:
                print("UI Session: Initalized")
            case .idle:
                print("UI Session: Idle")
            case .active:
                print("UI Session: Active Workout")
            case .complete:
                print("UI Session: Completed Workout")
            default:
                print("UI Session: Unknown")
            }
        }
        
    }
    func didUpdateWorkoutState(new: WorkoutSessionState, prev: WorkoutSessionState) {
        DispatchQueue.main.async {
            switch new {
            case .running:
                print("UI Workout: Running")
            case .paused:
                print("UI Workout: Paused")
            case .idle:
                print("UI Workout: Idle")
            case .stopped:
                print("UI Workout: Stopped")
            case .none:
                print("UI Workout: None")
            default:
                print("UI Workout: Unknown")
            }
        }
    }
    
    func didUpdateHeartbeatzBLEState(state: HeartbeatzBLEState) {
        //Note: The framework handles its own connection and reconnect functions internally. This delegate is so the developer can inform the UI if the case warrants. SessionState is the main control of screens and overall state of the framework. This informs the state of the actual heartbeatz device or service.
        switch state {
        case .connected:
            print("Heartbeatz Device: Connected")
        case .connecting:
            print("Heartbeatz Device: Connecting")
        case .disconnected:
            print("Heartbeatz Device: Disconnected")
        default:
            print("Heartbeatz Device: Unknown")
        }
    }
    
    
    //MARK: - DELEGATE LONG MESSAGE
    func didReceiveMessage(message: String, key: Int, state: LongMessageState, error: LongMessageStateError?) {
        DispatchQueue.main.async {
            if state == .received {
                print("UI Long Message: \(message) Key: \(key)")
            } else {
                if error == nil {
                    print("UI Long Receive State: \(state)")
                } else {
                    print("UI Long Receive State: \(state) Error: \(String(describing: error))")
                }
            }
        }
    }
    
    func didSendMessage(state: LongMessageState, error: LongMessageStateError?) {
        DispatchQueue.main.async {
            if error == nil {
                print("UI Long Send State: \(state)")
            } else {
                print("UI Long Send State: \(state) Error: \(String(describing: error))")
            }
        }
    }
    
}
    
