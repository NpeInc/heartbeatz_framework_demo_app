//
//  InterfaceController.swift
//  HeartbeatzWatchDemo Extension
//
//  Created by Steffin Fox Griswold on 8/13/21.
//

import WatchKit
import Foundation
import HeartbeatzFW

class InterfaceController: WKInterfaceController {

    @IBOutlet var hr: WKInterfaceLabel!
    @IBOutlet var action: WKInterfaceButton!
    
    var actionState: Bool = false
    let connectionModel = ConnectionModel.sharedConnectionModel
    var displayTimer = Timer()
    
    var bpm = "0"
    var tCal = "0"
    var aCal = "0"
    var dvCal = "0"
    var tDist = "0"
    
    // not enough room to show on screen in simple format
    var avgHR = "0"
    var instSpeed = "0"
    var avgBikeSpeed = "0"
    var avgBikeCadence = "0"
    var cadence = "0"
    var power = "0"
    var avgPower = "0"
    var count: Int = 0
    
    
    override func awake(withContext context: Any?) {
        //configure the framework
        connectionModel.scanForDeviceTypes = [.heartbeatzLib]

        connectionModel.minRssi = -80
        connectionModel.minProxyRssi = -50
        connectionModel.showCommsPrint = true
        connectionModel.showDebugPrint = false
        
        DispatchQueue.main.async {
            //NOTE: if using the multiDevice handler set true, else false to use autoconnect to any Heartbeatz device
            self.connectionModel.discoverOrConnnectHeartbeatz(useSelectedUserDevice: false)
        }
        
        //configure notifications
        NotificationCenter.default.addObserver(self, selector: #selector(checkSessionState), name: Notification.Name("sessionStateEvent"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(workoutDiscardEvent), name: Notification.Name("workoutDiscardEvent"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(workoutSaveEvent), name: Notification.Name("workoutSaveEvent"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(workoutStart(notification:)), name: Notification.Name("workoutStartEvent"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(workoutEnd(notification:)), name: Notification.Name("workoutEndEvent"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(workoutPause(notification:)), name: Notification.Name("workoutPauseEvent"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(workoutResume(notification:)), name: Notification.Name("workoutResumeEvent"), object: nil)
        
        //display timer
        displayTimer.invalidate()
        displayTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { Timer in
            self.didUpdateMetrics()
        }
    }
    
    @IBAction func tapAction() {
        if actionState {
            // reconnect
            self.action.setTitle("Disconnect")
            connectionModel.resetDisconnectHeartbeatz()
            actionState = false
        } else {
            // disconnect
            self.action.setTitle("Reconnect")
            connectionModel.disconnectHeartbeatz(stopScanning: false, delay: 0.0, saveWorkout: false)
            actionState = true
        }
    }
    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        self.action.setTitle("Disconnect")
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }
    
    //MARK: - ACTIONS
    func didUpdateMetrics(){
        let workoutState = self.connectionModel.workoutSessionState
        if workoutState == .running {
            bpm = ConnectionModel.getHrValue(source: connectionModel.phoneCurrHr)
            
            tCal = ConnectionModel.getCalorieValue(source: connectionModel.totalCalories)
            aCal = ConnectionModel.getCalorieValue(source: connectionModel.activeCalories)
            dvCal = ConnectionModel.getCalorieValue(source: connectionModel.deviceTotalCalories)
            tDist = ConnectionModel.getDistanceValue(source: connectionModel.totalDistance)
            
            // not enough room to show on screen in simple format
            avgHR = ConnectionModel.getHrValue(source: connectionModel.currAvgHr)
            instSpeed = ConnectionModel.getSpeedValue(source: connectionModel.instantSpeed)
            avgBikeSpeed = ConnectionModel.getSpeedValue(source: connectionModel.avgBikeSpeed)
            avgBikeCadence = ConnectionModel.getCadenceValue(source: connectionModel.avgBikeCadence)
            cadence = ConnectionModel.getCadenceValue(source: connectionModel.cadence)
            power = ConnectionModel.getPowerValue(source: connectionModel.instantPower)
            avgPower = ConnectionModel.getPowerValue(source: connectionModel.avgPower)
            
            hr.setText("BPM: \(bpm) \nState: \(workoutState) \naCal: \(aCal) \ntCal: \(tCal) \ntDist: \(tDist)")
            
            print("BPM: \(bpm) State: \(workoutState) Cal: \(aCal) tCal: \(tCal) dvCal: \(dvCal) tDist: \(tDist) \nExtra: avgHR: \(avgHR) speed: \(instSpeed) avgSpeed: \(avgBikeSpeed) rpm: \(cadence) avgRpm: \(avgBikeCadence) power: \(power) avgPower: \(avgPower)")
        } else if workoutState == .idle {
            bpm = ConnectionModel.getHrValue(source: connectionModel.phoneCurrHr)
            hr.setText("Workout Idle: \nState: \(workoutState) \nBPM: \(bpm) ")
        }

    }
    @objc func checkSessionState() {
        switch connectionModel.sessionState {
        case .scanning:
            print("UI Session: Scanning")
            self.presentController(withName: "Scanner", context: nil)
        case .initializing:
            print("UI Session: Initalizing")
            
            hr.setText("Initalizing")
        case .initialized:
            print("UI Session: Initalized")
            
            hr.setText("Initalized")
        case .idle:
            print("UI Session: Idle")
            
            hr.setText("Idle")
        case .active:
            print("UI Session: Active Workout")
        case .complete:
            print("UI Session: Completed Workout")
        default:
            print("UI Session: Unknown")
        }
    }
    
    @objc func workoutStart(notification: Notification){
        print("workoutStart")
    }
    
    @objc func workoutEnd(notification: Notification){
        print("workoutEnd")
    }
    
    @objc func workoutPause(notification: Notification){
        print("workoutPause")
    }
    
    @objc func workoutResume(notification: Notification){
        print("workoutResume")
    }
    @objc func workoutDiscardEvent() {
        print("workoutDiscardEvent")
    }
    
    @objc func workoutSaveEvent() {
        print("Workout Save Event")
        if ConnectionModel.sharedConnectionModel.currentSummary != nil {
            let workoutSummary = ConnectionModel.sharedConnectionModel.currentSummary!
            self.hr.setText("Summary: title: \(workoutSummary.title) \nTime: \(workoutSummary.time) \nHR: \(workoutSummary.avgHeartRate) \nAvg Speed: \(workoutSummary.avgBikeSpeed)")
        }
    }

}
