//
//  MenuContoller.swift
//  HeartbeatzWatchDemo Extension
//
//  Created by Steffin Fox Griswold on 11/16/21.
//

import Foundation
import HeartbeatzFW
import WatchKit
import HealthKit

class MenuController: HeartbeatzDataView {
    @IBOutlet var table: WKInterfaceTable!
    
    enum MenuAction {
        case pause
        case stop
        case resume
        case start
        case send
        case disconnect
        case none
    }
    
    struct MenuData{
        let icon: UIImage?
        let label:  String
        let action: MenuAction
        let workoutType: WorkoutType?
    }
    
    var menuData: [MenuData] = []
    
    
    override func willActivate() {
        DispatchQueue.main.async {
            self.connectionModel.delegate = self
            if self.connectionModel.heartbeatzBLEState == .unknown || self.connectionModel.heartbeatzBLEState == .disconnected {
                DispatchQueue.main.async {
                    //NOTE: if using the multiDevice handler set true, else false to use autoconnect to any Heartbeatz device
                    //If MultiDevice class is used to set the device to connect to, this function will initate the connection.
                    print("Attempt to connect via main")
                    self.connectionModel.discoverOrConnnectHeartbeatz(useSelectedUserDevice: true)
                }
            }
            
            self.updateMenu()
        }
    }
    
    func updateMenu(){
        DispatchQueue.main.async {
            switch self.connectionModel.sessionState {
            case .active:
                switch self.connectionModel.workoutSessionState{
                case .running, .paused:
                    self.showActiveWorkoutDisplay()
                default:
                    self.showStartWorkoutDisplay()
                }
            case .initializing, .none, .scanning:
                print("Menu Conctrol Starting")
                self.showStartupDisplay()
            default:
                self.showStartWorkoutDisplay()
            }
        }
        
    }
    
    //MARK: - Display Table Options
    func showStartWorkoutDisplay() {
            self.menuData = []
            var type = WorkoutType(HKWorkoutActivityType.mixedCardio, location: HKWorkoutSessionLocationType.indoor)
            var row = MenuData(icon: UIImage(named: "wk_cardio"), label: "Mixed Cardio", action: .start, workoutType: type)
            self.menuData.append(row)
            
            type = WorkoutType(HKWorkoutActivityType.cycling, location: HKWorkoutSessionLocationType.outdoor)
            row = MenuData(icon: UIImage(named: "wk_bike"), label: "Outdoor Cycle", action: .start, workoutType: type)
            self.menuData.append(row)
            
            type = WorkoutType(HKWorkoutActivityType.cycling, location: HKWorkoutSessionLocationType.indoor)
            row = MenuData(icon: UIImage(named: "wk_indoorBike"), label: "Indoor Cycle", action: .start, workoutType: type)
            self.menuData.append(row)
            
            type = WorkoutType(HKWorkoutActivityType.running, location: HKWorkoutSessionLocationType.indoor)
            row = MenuData(icon: UIImage(named: "wk_tread"), label: "Indoor Running", action: .start, workoutType: type)
            self.menuData.append(row)
            
            type = WorkoutType(HKWorkoutActivityType.walking, location: HKWorkoutSessionLocationType.indoor)
            row = MenuData(icon: UIImage(named: "wk_treadWalk"), label: "Indoor Walking", action: .start, workoutType: type)
            self.menuData.append(row)
            
            type = WorkoutType(HKWorkoutActivityType.rowing, location: HKWorkoutSessionLocationType.indoor)
            row = MenuData(icon: UIImage(named: "wk_rowing"), label: "Indoor Rowing", action: .start, workoutType: type)
            self.menuData.append(row)
            
            row = MenuData(icon: UIImage(named: "disconnect"), label: "Disconnect", action: .disconnect, workoutType: nil)
            self.menuData.append(row)
            
            self.displayTable()
        
    }
    
    func showActiveWorkoutDisplay() {
        DispatchQueue.main.async {
            self.menuData = []
            //pause or resume
            var row = MenuData(icon: UIImage(named: "pause"), label: "Pause Workout", action: .pause, workoutType: nil)
            if self.connectionModel.workoutSessionState == .paused {
                row = MenuData(icon: UIImage(named: "resume"), label: "Resume Workout", action: .resume, workoutType: nil)
            }
            self.menuData.append(row)
            // stop
            row = MenuData(icon: UIImage(named: "stop"), label: "Stop Workout", action: .stop, workoutType: nil)
            self.menuData.append(row)
            
            row = MenuData(icon: UIImage(named: "stop"), label: "Send Message", action: .send, workoutType: nil)
            self.menuData.append(row)
            
            row = MenuData(icon: UIImage(named: "disconnect"), label: "Disconnect", action: .disconnect, workoutType: nil)
            self.menuData.append(row)
            
            self.displayTable()
        }
    }
    
    func showStartupDisplay() {
        DispatchQueue.main.async {
            self.menuData = []
            var row = MenuData(icon: UIImage(named: "wk_cardio"), label: "Initializing", action: .none, workoutType: nil)
            self.menuData.append(row)
            
            row = MenuData(icon: UIImage(named: "disconnect"), label: "Disconnect", action: .disconnect, workoutType: nil)
            self.menuData.append(row)
            
            self.displayTable()
        }
    }
        
    func displayTable(){
        DispatchQueue.main.async {
            if self.menuData.first?.action == MenuController.MenuAction.none {
                self.table.setNumberOfRows(self.menuData.count, withRowType: "message")
            } else {
                self.table.setNumberOfRows(self.menuData.count, withRowType: "menu")
            }
            
            for index in 0..<self.table.numberOfRows {
                let controller = self.table.rowController(at: index) as? MenuRow
                controller?.icon.setImage(self.menuData[index].icon)
                controller?.label.setText(self.menuData[index].label)
            }
        }
        
    }
    
    //MARK: - Control workout Options
    func pauseWorkout(){
        connectionModel.pauseWorkout()
    }
    func resumeWorkout(){
        connectionModel.resumeWorkout()
    }
    func stopWorkout(){
        connectionModel.stopWorkout()
    }
    func startWorkout(type: WorkoutType){
        DispatchQueue.main.async {
            let started = self.connectionModel.configureWorkout(currentWorkoutType: type)
            print("Menu requested start workout: \(started)")
            
            // for demonstration purpose. String is limited to 500 characters and JSON format is exceptable
            self.connectionModel.sendLongMessageToLibrary(message: "the watch is now starting a workout type: \(type)", key: 24, completion: {success in
                print("Sending a long message \(success)")
            })
            
            // start workout and jump to display screens
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
                WKInterfaceController.reloadRootPageControllers(withNames: ["MenuController","WorkoutController","WorkoutController2","SummaryController"], contexts: nil, orientation: .horizontal, pageIndex: 1)
            }
            
        }
    }
    func disconnectHeartbeatz(){
        print("Menu requested Disconnect")
        self.connectionModel.disconnectHeartbeatz(stopScanning: true, delay: 0, saveWorkout: true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.15) {
            WKInterfaceController.reloadRootPageControllers(withNames: ["Scanner"], contexts: nil, orientation: .horizontal, pageIndex: 0)
        }
        
        
    }
    
    //MARK: - Communicate with Library Options
    func sendMessage(message:String, key: Int){
        connectionModel.sendLongMessageToLibrary(message: message, key: key) { success in
            print("Sent Message: \(success)")
        }
    }
    //MARK: - Table Delegate
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        DispatchQueue.main.async {
            let selected = self.menuData[rowIndex]
            switch selected.action {
            case .start:
                self.startWorkout(type: selected.workoutType!)
            case .pause:
                self.pauseWorkout()
            case .stop:
                self.stopWorkout()
            case .resume:
                self.resumeWorkout()
            case .send:
                self.sendMessage(message: "test", key: 1)
            case .disconnect:
                self.disconnectHeartbeatz()
            case .none:
                break
            }
        }
    }
    
    //MARK: - Delegate Options
    override func didUpdateSessionState(new: SessionState, prev: SessionState) {
        print("Did update session state: current: \(new) previous: \(prev)")
        DispatchQueue.main.async {
            self.updateMenu()
        }
    }
    override func didUpdateWorkoutState(new: WorkoutSessionState, prev: WorkoutSessionState) {
        print("Did update workout session state: current: \(new) previous: \(prev)")
        DispatchQueue.main.async {
            switch new {
            case .running:
                print("UI Workout: Running")
            case .paused:
                print("UI Workout: Paused")
            case .idle:
                print("UI Workout: Idle")
            case .stopped:
                print("UI Workout: Stopped")
            case .none:
                print("UI Workout: None")
            default:
                print("UI Workout: Unknown")
            }
        }
    }
    
   
    
}
