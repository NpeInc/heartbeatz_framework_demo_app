//
//  InterfaceController.swift
//  HeartbeatzWatchDemo Extension
//
//  Created by Steffin Fox Griswold on 8/13/21.
//

import WatchKit
import Foundation
import HeartbeatzFW

class MainDisplayController: HeartbeatzDataView {
    
    @IBOutlet var hr: WKInterfaceLabel!
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        print("Main Screen Active")
        DispatchQueue.main.async {
            self.connectionModel.delegate = self
            self.didUpdateMetrics()
        }

    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }
    override func displayMetrics(){
        if workoutState == .running {
            mainScreenDisplay = "Time: \(workoutTime)\nBPM: \(bpm) \nState: \(workoutState) \naCal: \(aCal) \ntCal: \(tCal) \ntDist: \(tDist)"
            
        } else if workoutState == .idle {
            bpm = ConnectionModel.getHrValue(source: connectionModel.phoneCurrHr)
            mainScreenDisplay = "Workout Starting: \nBPM: \(bpm) "
        } else {
            let sessionState = self.connectionModel.sessionState
            mainScreenDisplay = "Session State \(sessionState)"
        }
        
        hr.setText(mainScreenDisplay)
    }
    
}

