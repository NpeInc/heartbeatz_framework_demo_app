//
//  SummaryController.swift
//  HeartbeatzWatchDemo Extension
//
//  Created by Steffin Fox Griswold on 11/16/21.
//

import Foundation
import HeartbeatzFW
import WatchKit

class SummaryController: HeartbeatzDataView {
    @IBOutlet var summary: WKInterfaceLabel!
    
    override func willActivate() {
        DispatchQueue.main.async { [self] in
            connectionModel.delegate = self
            
            if let data = connectionModel.currentSummary {
                summaryScreenDisplay = "Activity: \(data.activity) \nTitle:\(data.title) \n"
                summaryScreenDisplay.append("Time: \(ConnectionModel.getTimeValue(source: data.time))\n")
                summaryScreenDisplay.append("Avg Hr: \(ConnectionModel.getHrValue(source: data.avgHeartRate))\n")
                summaryScreenDisplay.append("Active Cal: \(ConnectionModel.getCalorieValue(source: data.activeCalories))\n")
                summaryScreenDisplay.append("Total Cal: \(ConnectionModel.getCalorieValue(source: data.activeCalories))\n")
                summaryScreenDisplay.append("Total Distance: \(ConnectionModel.getDistanceValue(source: data.totalDistance))\n")
                summaryScreenDisplay.append("Avg Speed: \(ConnectionModel.getSpeedValue(source: data.avgBikeSpeed))\n")
                summaryScreenDisplay.append("Avg RPM: \(ConnectionModel.getCadenceValue(source: data.avgBikeCadence))\n")
                
            } else {
                summaryScreenDisplay = "No workout summary to display"
            }
            
            summary.setText(summaryScreenDisplay)
        }
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }

}
