//
//  ScannerController.swift
//  HeartbeatzWatchDemo Extension
//
//  Created by Steffin Fox Griswold on 9/29/21.
//

import Foundation

import HeartbeatzFW
import WatchKit

enum DeviceAvailabiltyState{
    case available
    case discovered
    case stored
    case unknown
}
struct RowData{
    let device: UserStoredDevices
    let state:  DeviceAvailabiltyState
}
class ScannerController: WKInterfaceController, MultiDeviceDelegate {
    @IBOutlet var table: WKInterfaceTable!

    var displayStack: [RowData] = []
    let multiDevice = MultiDevice.shared
    
    override func awake(withContext context: Any?) {
        print("Scan")
        multiDevice.delegate = self
        multiDevice.minRssi = -80
        multiDevice.scanForDeviceTypes = [.heartbeatz, .heartbeatzLib, .heartbeatzCable, .heartbeatzWyûrUSB, .heartbeatzHbtzUSB]
    }
    override func willActivate() {
        // starts scanning for heartbeatz devices/library
        multiDevice.startListening()
        
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }
    
    func didDiscoverDevices(availableList: [UserStoredDevices], savedList: [UserStoredDevices], discoveredList: [UserStoredDevices], current: UserStoredDevices?) {
        DispatchQueue.main.async { [self] in
            displayStack = []
            for data in availableList{
                displayStack.append(RowData(device: data, state: .available))
            }
            for data in discoveredList{
                displayStack.append(RowData(device: data, state: .discovered))
            }
            for data in savedList{
                displayStack.append(RowData(device: data, state: .stored))
            }
            
            table.setNumberOfRows(self.displayStack.count, withRowType: "scanRow")
            for index in 0..<table.numberOfRows {
                let controller = table.rowController(at: index) as? ScannerRow
                
                controller?.state = self.displayStack[index].state
                controller?.device = self.displayStack[index].device
                controller?.populate()
            }
        }
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        DispatchQueue.main.async { [self] in
            let state = self.displayStack[rowIndex].state
            print("Tapped \(rowIndex) state: \(self.displayStack[rowIndex].state)")
            if state == .available || state == .discovered {
                // Saves selected device internally and prepares for jump to workout controller.
                self.multiDevice.saveSelectedDeviceAndStopListening(selected: self.displayStack[rowIndex].device)
                
                WKInterfaceController.reloadRootPageControllers(withNames: ["MenuController"], contexts: nil, orientation: .horizontal, pageIndex: 0) //
                
            }
        }
    }
}

