//
//  InterfaceController.swift
//  HeartbeatzWatchDemo Extension
//
//  Created by Steffin Fox Griswold on 8/13/21.
//

import WatchKit
import Foundation
import HeartbeatzFW

class ExtraDisplayController: HeartbeatzDataView {
    @IBOutlet var moreMetrics: WKInterfaceLabel!
    

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        print("Extra Screen active")
        DispatchQueue.main.async {
            self.connectionModel.delegate = self
            self.didUpdateMetrics()
        }
       
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }
    
    override func displayMetrics(){
        if workoutState == .running {
            if self.connectionModel.workoutType?.activity == .rowing {
                extraScreenDisplay = "avgHR: \(avgHR) stroke Count: \(strokeCount) instPace: \(instPace) avgPace: \(avgPace) avgCadence: \(avgCadence)"
            } else {
                extraScreenDisplay = "avgHR: \(avgHR) speed: \(instSpeed) avgSpeed: \(avgBikeSpeed) rpm: \(cadence) avgRpm: \(avgBikeCadence) power: \(power) avgPower: \(avgPower)"
            }
            
        } else if workoutState == .idle {
            extraScreenDisplay = "Workout is idle, no data to display..."
        } else {
            let sessionState = self.connectionModel.sessionState
            extraScreenDisplay = "Session State \(sessionState)"
        }
        
        moreMetrics.setText(extraScreenDisplay)
    }
    
}

