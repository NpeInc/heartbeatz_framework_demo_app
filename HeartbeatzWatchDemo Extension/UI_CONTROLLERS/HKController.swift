//
//  HKController.swift
//  HeartbeatzWatchDemo Extension
//
//  Created by Steffin Fox Griswold on 9/7/21.
//

import Foundation
import HeartbeatzFW
import WatchKit

class HKController: WKInterfaceController {
    @IBOutlet var info: WKInterfaceLabel!
    @IBOutlet var action: WKInterfaceButton!
    
    
    let npePermissions = HBTZPermissions.shared
    override func awake(withContext context: Any?) {
        
        // permissions to ask
        if !npePermissions.shownHKPermissions {
            
            info.setText("OnBoard HealthKit: \nShown: \(npePermissions.shownHKPermissions)")
            npePermissions.HKPremissionsWrite = [.workoutInfo, .distanceCycle, .distanceTreadmill]
            npePermissions.HKPremissionsRead = [.workoutInfo, .heartrate, .activeEnergy, .basilEnergy, .distanceCycle, .distanceTreadmill]
            
            npePermissions.setupHealthKit { [self] success in
                if success {
                    print("Has user been shown Permissions: \(success)")
                    info.setText("OnBoard HealthKit: /nConfig: \(success)")
                    npePermissions.requestPermissions { success, error in
                        print("Request Permissions: \(success) errors: \(String(describing: error))")
                        if success {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                npePermissions.queryPermissionStatus { success, errors in
                                    var message = "OnBoard HealthKit: \nQuery: \(success) "
                                    print("Query Permissions: \(success)")
                                    if errors.count > 0 {
                                        for item in errors {
                                            print("Query Permissions: Error: \(item)")
                                            message += "\(item),\n"
                                        }
                                    }
                                    info.setText(message)
                                } // end query
                            }
                            
                        }
                    } // end request
                }
            } // end setup
        }
        
        
    }
    override func willActivate() {
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
    }
}
